#include <WiFi.h>
#include <GyverHTU21D.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>

#define SENSOR_FETCH_DELAY 100

const char* ssid = "PUT_SSID_HERE"; 
const char* password = "PASSWORD_HERE";
unsigned long previousMillis = 0;
unsigned long interval = 500; // Default interval
GyverHTU21D htu;
AsyncWebServer server(80);

float temperature = 0.0;
float humidity = 0.0;

void customDelay(unsigned long delayTime) {
    unsigned long startTime = millis();
    while (millis() - startTime < delayTime) {
    }
}

void printTemperatureAndHumidity(float temperature, float humidity){
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.print(" °C, Humidity: ");
    Serial.print(humidity);
    Serial.println(" %");
}

void getTemperatureAndHumidityWait(float &temperature, float &humidity){
    htu.requestTemperature();
    customDelay(SENSOR_FETCH_DELAY);
    if (htu.readTemperature()) {
        temperature = htu.getTemperature();
    }

    htu.requestHumidity();
    customDelay(SENSOR_FETCH_DELAY);
    if (htu.readHumidity()) {
        humidity = htu.getHumidity();
    }
}

void setup() {
    Serial.begin(9600);
    htu.begin();

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println("Connecting to WiFi...");
    }
    Serial.println("Connected to WiFi");

    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        // Request sensor data
        getTemperatureAndHumidityWait(temperature, humidity);
        String response = String(temperature) + " " + String(humidity);
        request->send(200, "text/plain", response);
        printTemperatureAndHumidity(temperature, humidity);
    });

    server.begin();
}

void loop() {
  delay(10);
}
