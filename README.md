# PPC ESP32 Meteostanice s Qt


## O Čem je tento projekt 
Jde o malou meteostanici komunikující s PC.  
Jeho hlavní součástí je ESP32 s připojeným senzorem HTU21D měřící teplotu a vlhkost vzduchu.   
Tyto informace poté posílá PC pomocí HTTP POST přes WiFi, data jsou zobrazena v grafickém rozhraní  na PC.   
***
# Video ukázka
https://www.youtube.com/watch?v=w9UfBXh4JqU

***
## Schéma zapojení 

![IMAGE_DESCRIPTION](https://i.imgur.com/ITpy9Em.png)
HTU21D využívá I2C pro komunikaci
***

##  PC panel
![IMAGE_DESCRIPTION](https://i.imgur.com/BQRANWT.png)
![IMAGE_DESCRIPTION](https://i.imgur.com/FkGYTlB.jpeg)


 ## Spuštění 
1. Stáhněte projekt
2. V **esp32_meteo_qt_code.ino** nastavte WiFi SSID a heslo
```
const  char* ssid = "PUT_SSID_HERE";
const  char* password = "PASSWORD_HERE";
```
3. Kompilujte a nahrajte na ESP32 
4. V Qt creatoru kompilujte a spusťte release verzi 
5. Připojte ESP32 k Seriové lince, při úspěšném připojení na wifi vypíše svoji IPv4 adresu
6. Zadejte adresu do pole pro IP adresu a zmáčkněte Connect
7. (Volitelné) Upravte interval aktualizací 

## Použití 

Projekt je určen hlavně pro pozorování změn teplot po delší dobu.
