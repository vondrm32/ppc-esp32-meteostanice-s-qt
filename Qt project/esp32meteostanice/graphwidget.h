#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QWidget>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QTimer>


class GraphWidget : public QWidget {
    Q_OBJECT

public:
    explicit GraphWidget(QWidget *parent = nullptr);
    ~GraphWidget();
    void updateGraph( double temper_or_humid);
    void setXAxisTitle(const QString &title);
    void setYAxisTitle(const QString &title);
    void setYAxisRange(qreal min, qreal max);
    void resetTime();

private:
    QChart *chart;
    QLineSeries *series;
    int time;
    double minY;
    double maxY;
};

#endif // GRAPHWIDGET_H
