#include "mainwindow.h"
#include "qtimer.h"
#include "ui_mainwindow.h"
#include <QNetworkRequest>
#include <QUrl>
#include <QPixmap>
#include <QDir>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
     setWindowTitle("ESP32 Meteostanice Panel");

    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, &QNetworkAccessManager::finished, this, &MainWindow::handleNetworkData);

    dataTimer = new QTimer(this);
    connect(dataTimer, &QTimer::timeout, this, &MainWindow::fetchSensorData);

    temperatureSeries = new QLineSeries();
    humiditySeries = new QLineSeries();

    ui->temper_widget->setXAxisTitle("Time [s]");
    ui->temper_widget->setYAxisTitle("Tepmerature [°C]");

    maxYrangetemp = 32;
    minYrangetemp = 30;
    ui->temper_widget->setYAxisRange(minYrangetemp, maxYrangetemp);


        ui->humid_widget->setXAxisTitle("Time [s]");
        ui->humid_widget->setYAxisTitle("Humidity [%]");
        ui->humid_widget->setYAxisRange(0, 100);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    const int default_interval = 5000;
    dataTimer->start(default_interval); // Default interval of 5 seconds
    ui->currentIntervalLabel->setText(QString::number(default_interval/1000));
    fetchSensorData();

}

void MainWindow::on_setIntervalButton_clicked()
{
    int interval = ui->intervalSpinBox->value();
    dataTimer->setInterval(interval*1000); //setInterval wants ms
    ui->currentIntervalLabel->setText(QString::number(interval));
    qDebug() << "Setting the interval in seconds as" << interval;


}

void MainWindow::fetchSensorData()
{
    QString ipAddress = ui->ipLineEdit->text();
    QString url = "http://" + ipAddress;
    QNetworkRequest request((QUrl(url)));
    networkManager->get(request);
}

void MainWindow::handleNetworkData(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError) {
        QByteArray responseData = reply->readAll();
        QString data = QString::fromUtf8(responseData).trimmed();
        QStringList dataList = data.split(" ");

        if (dataList.size() == 2) {
            double temperature = dataList[0].toDouble();
            double humidity = dataList[1].toDouble();

            if ((temperature == 0 && humidity == 0)) { // very low change of this happening unless bad response
                reply->deleteLater();
                return;
            }

            ui->temperatureLabel->setText(QString::number(temperature) + " °C");
            ui->humidityLabel->setText(QString::number(humidity) + " %");
            qDebug() << "Updating temperature graph";
            ui->temper_widget->updateGraph( temperature);
            ui->humid_widget->updateGraph(humidity);

            if(temperature+1 > maxYrangetemp){
                maxYrangetemp = temperature +1;
                ui->temper_widget->setYAxisRange(minYrangetemp, maxYrangetemp);
            }
            if(temperature-1 < minYrangetemp){
                minYrangetemp = temperature - 1;
                ui->temper_widget->setYAxisRange(minYrangetemp, maxYrangetemp);
            }


            qDebug() << "Updating humidity graph" << "its about to be" << humidity;



        }
    }
    reply->deleteLater();
}

