#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QChartView>
#include <QLineSeries>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_connectButton_clicked();
    void on_setIntervalButton_clicked();
    void fetchSensorData();
    void handleNetworkData(QNetworkReply *reply);

private:

    Ui::MainWindow *ui;
    QNetworkAccessManager *networkManager;
    QLineSeries *temperatureSeries;
    QLineSeries *humiditySeries;
    double maxYrangetemp;
    double minYrangetemp;
    QTimer* dataTimer;
};

#endif // MAINWINDOW_H
