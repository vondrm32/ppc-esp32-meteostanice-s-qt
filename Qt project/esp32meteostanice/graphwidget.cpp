#include "graphwidget.h"
#include <QtCharts/QValueAxis>
#include <QVBoxLayout>
#include <QTimer>

GraphWidget::GraphWidget(QWidget *parent) :
    QWidget(parent),
    chart(new QChart),
    series(new QLineSeries),
    time(0),
    minY(-10),
    maxY(50)
{
    // Set up chart
    chart->legend()->hide();
    chart->addSeries(series);

    /*QPen pen(QColor("#ff0000")); // Red color
    pen.setWidth(2); //
    series->setPen(pen);*/

    QValueAxis *axisX = new QValueAxis;
    axisX->setTickCount(10);
    axisX->setLabelFormat("%d");
    axisX->setTitleText("VALUE_x");
    axisX->setRange(0, 10); // Initial range from 0 to 10
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setRange(-10, 50);  // Temperature range
    axisY->setTitleText("VALUE_Y");
    axisY->setLabelFormat("%.1f");
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    // Set up chart view
    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    // Layout for the graphwidget
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(chartView);
    setLayout(layout);


}

GraphWidget::~GraphWidget() {
    delete chart;
    delete series;
}

void GraphWidget::resetTime(){
    time = 0;
}

void GraphWidget::updateGraph(double temper_or_humid) {
    time++;

    series->append(time, temper_or_humid);

     qDebug() << "Updating graph with elapsed time:" << time << "and value:" << temper_or_humid;



    QList<QAbstractAxis *> axes = chart->axes(Qt::Horizontal);
    if (!axes.isEmpty()) {
        QValueAxis *axisX = static_cast<QValueAxis *>(axes.first());
        const int GRAPHEXPANDCONST = 20;
        if (time > GRAPHEXPANDCONST) {
            axisX->setRange(time- GRAPHEXPANDCONST, time+1);
        } else { axisX->setRange(0, GRAPHEXPANDCONST); //First time run

        }
    }



}

void GraphWidget::setXAxisTitle(const QString &title) {
    QList<QAbstractAxis *> axes = chart->axes(Qt::Horizontal);
    if (!axes.isEmpty()) {
        QValueAxis *axisX = static_cast<QValueAxis *>(axes.first());
        axisX->setTitleText(title);
    }
}

void GraphWidget::setYAxisTitle(const QString &title) {
    QList<QAbstractAxis *> axes = chart->axes(Qt::Vertical);
    if (!axes.isEmpty()) {
        QValueAxis *axisY = static_cast<QValueAxis *>(axes.first());
        axisY->setTitleText(title);
    }
}

void GraphWidget::setYAxisRange(qreal min, qreal max) {
    QList<QAbstractAxis *> axes = chart->axes(Qt::Vertical);
    if (!axes.isEmpty()) {
        QValueAxis *axisY = static_cast<QValueAxis *>(axes.first());
        axisY->setRange(min, max);
    }
}
